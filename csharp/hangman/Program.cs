﻿using System;

namespace hangman
{
    class Program
    {
        const int MaxNumGuesses = 7;

        static void Main(string[] args)
        {
            var rnd = new Random();
            var month = rnd.Next(1, 13);
            var secret = Secrets.Data[rnd.Next(0, Secrets.Data.Count)];

            var gameResult = Hangman.MakeAGuess(MaxNumGuesses, secret, "", Hangman.GetGuess, Hangman.EvaluateGuess);
            switch (gameResult) {
                case Hangman.Game.HasQuit: return;
                case Hangman.Game.HasWon: Console.WriteLine($"the word was \"{secret}\". you have won!!"); return;
                default: Console.WriteLine($"the word was \"{secret}\". you lost :("); return;
            }
        }
    }
}
