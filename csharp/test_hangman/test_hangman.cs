﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using hangman;

namespace test_hangman
{
    public class DescribeHangman
    {
        [TestClass]
        public class GuessesCanBeEvaluatedCorrectly : DescribeHangman
        {
            [TestMethod]
            public void WhenTheGuessIsValidReturnValid()
            {
                Assert.AreEqual(Hangman.EvaluateGuess('t', "string", ""), Hangman.Guess.Valid);
            }

            [TestMethod]
            public void WhenTheGuessIsNotValidReturnInvalid()
            {
                Assert.AreEqual(Hangman.EvaluateGuess('c', "string", ""), Hangman.Guess.Invalid);
            }

            [TestMethod]
            public void WhenTheGuessHasAlreadyBeenTriedReturnAlreadyTested()
            {
                Assert.AreEqual(Hangman.EvaluateGuess('t', "string", "tr"), Hangman.Guess.AlreadyTested);
            }
        }

        [TestClass]
        public class CanPlayGameAccordingToRules
        {
            [TestMethod]
            public void WhenTooManyBadGuessesMadeWeLose()
            {
                var secret = "this is a secret";

                Assert.AreEqual(
                    Hangman.Game.HasLost,
                    Hangman.MakeAGuess(7, secret, "", (_1, _2) => 'z', (_1, _2, _3) => Hangman.Guess.Invalid));
            }

            [TestMethod]
            public void WhenLotsOfGoodGuessesAreMadeWeWin()
            {
                var secret = "this is a secret";
                var guessCount = 0;
                Func<string, int, char?> guessFn = (_1, _2) => secret[guessCount++];

                Assert.AreEqual(
                    Hangman.Game.HasWon,
                    Hangman.MakeAGuess(7, secret, "", guessFn, (_1, _2, _3) => Hangman.Guess.Valid));
            }

            [TestMethod]
            public void WhenSixBadGuessesMadeWeWin()
            {
                var secret = "this is a secret";
                var guessCount = 0;
                var guessList = "bdfgjk acehirst";
                Func<string, int, char?> guessFn = (_1, _2) => guessList[guessCount++];

                Assert.AreEqual(
                    Hangman.Game.HasWon,
                    Hangman.MakeAGuess(7, secret, "", guessFn, Hangman.EvaluateGuess));
            }

            [TestMethod]
            public void WhenSevenBadGuessesMadeWeLose()
            {
                var secret = "this is a secret";
                var guessCount = 0;
                var guessList = "bdfgjkl acehirst";
                Func<string, int, char?> guessFn = (_1, _2) => guessList[guessCount++];

                Assert.AreEqual(
                    Hangman.Game.HasLost,
                    Hangman.MakeAGuess(7, secret, "", guessFn, Hangman.EvaluateGuess));
            }
        }

        [TestClass]
        public class CanMaskOutCharacters
        {
            [TestMethod]
            public void WhenStringIsMaskedGetExpected()
            {
                var toMask = "this is a string";
                var filter = "is";
                Assert.AreEqual("..is.is...s..i..", Hangman.MaskIfNotFound(toMask, filter));
            }
        }
    }
}
